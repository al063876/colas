package colas;

import java.util.ArrayList;

import java.util.Scanner;


public class Ejercicio3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
       ArrayList<String> letras = new ArrayList<>();
       ArrayList<String> numeros = new ArrayList<>();
        
        letras.add("A");
        letras.add("B");
        letras.add("C");
        letras.add("D");
        letras.add("E");
        letras.add("F");
        letras.add("G");
        letras.add("H");
        letras.add("I");
        letras.add("J");
        letras.add("K");
        letras.add("L");
        letras.add("M");
        letras.add("N");
        letras.add("O");
        letras.add("P");
        letras.add("Q");
        letras.add("R");
        letras.add("S");
        letras.add("T");
        letras.add("U");
        letras.add("V");
        letras.add("W");
        letras.add("X");
        letras.add("Y");
        letras.add("Z");
        
         //numeros
        numeros.add("97");
        numeros.add("98");
        numeros.add("99");
        numeros.add("100");
        numeros.add("101");
        numeros.add("102");
        numeros.add("103");
        numeros.add("104");
        numeros.add("105");
        numeros.add("106");
        numeros.add("107");
        numeros.add("108");
        numeros.add("109");
        numeros.add("110");
        numeros.add("111");
        numeros.add("112");
        numeros.add("113");
        numeros.add("114");
        numeros.add("115");
        numeros.add("116");
        numeros.add("117");
        numeros.add("118");
        numeros.add("119");
        numeros.add("120");
        numeros.add("121");
        numeros.add("122");
        System.out.println("");
        System.out.println("Sanchez Rosario Ricarod Daniel 63876");
        System.out.println("Tercer ejercicio");
        System.out.println("Letras del alfabeto, numero de caracter ASCII");
        for(int i=0;i<26;i++){
        System.out.println(letras.get(i)+" = "+numeros.get(i));
        
        } 
        Scanner sc = new Scanner(System.in);
        
        
        System.out.println(""); 
        
        System.out.println("INGRESE UN NUMERO DE LA TABLA ASCII");
        
        int numero = sc.nextInt();
        
        String binario = "";
        
        if(numero>0){
            
            while (numero>0){
                
                if (numero % 2 ==0){
                    
                    binario = "0" + binario;
                    
                }else{
                    
                    binario = "1" + binario;
                    
                }
                
                numero = (int) numero / 2;
                 
            }
       
        }else if (numero==0){
            
            binario = "0";
       
        
        } else {
            
            binario = "No se pudo convertir el numero";
        }
        
        
        System.out.println(""); 
       
    System.out.println("El numero en binario es : " + binario);  
    
}
    }
