//Author Ricardo Sanchez 
package colas;

public class Ejercicio4 {
    public static void main(String[] args) {
        System.out.println("Sanchez Rosario Ricardo Daniel 63876");
        System.out.println("Posición Segura "+getSafePosition(39));
    }

    public static int getSafePosition(int n) {
        int valueOfL = n - Integer.highestOneBit(n);
        System.out.println("Personas "+n);
        System.out.println("Binario  "+Integer.highestOneBit(n));
        System.out.println("Sobrante "+valueOfL);
        int safePosition = 2 * valueOfL  + 1;
        return safePosition;
    }
}